﻿using System;
using BenihanaEngineDX11.System;

namespace BenihanaEngineDX11
{
    static class Program
    {
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main()
        {
            BSystem.StartRenderForm();
        }
    }
}
