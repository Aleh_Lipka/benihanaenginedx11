﻿using System.Drawing;
using System.Windows.Forms;

using SharpDX.Windows;

using BenihanaEngineDX11.Input;
using BenihanaEngineDX11.Graphics;
using BenihanaEngineDX11.TestConsole;

using BenihanaEngineDX11.Sound;

namespace BenihanaEngineDX11.System
{
    class BSystem
    {
        // Properties
        private RenderForm RenderForm { get; set; }
        public BSystemConfiguration Configuration { get; private set; }
        public BInput Input { get; private set; }
        public BGraphics Graphics { get; private set; }
        public BSound Sound { get; private set; }
        public BFPS FPS { get; private set; }
        public BCPU CPU { get; private set; }
        public BTimer Timer { get; private set; }
        public BPosition Position { get; private set; }

        // Statuc Properties
        public static bool IsMouseOffScreen { get; set; }

        // Constructor
        public BSystem() { }

        // Methods
        public static void StartRenderForm(string title = "BeniganaEngine", int width = 800, int height = 600, bool vSync = false, bool fullScreen = true, int testTimeSeconds = 0)
        {
            BSystem system = new BSystem();
            system.Initialize(title, width, height, vSync, fullScreen, testTimeSeconds);
            system.RunRenderForm();
        }

        public virtual bool Initialize(string title, int width, int height, bool vSync, bool fullScreen, int testTimeSeconds)
        {
            bool result = false;

            if (Configuration == null)
            {
                Configuration = new BSystemConfiguration(title, width, height, fullScreen, vSync);
            }

            InitializeWindow(title);

            // Just for visuality
            RenderForm.BackColor = Color.Black;

            if (Input == null)
            {
                Input = new BInput();
                if (!Input.Initialize(Configuration, RenderForm.Handle))
                {
                    return false;
                }
            }

            if (Graphics == null)
            {
                Graphics = new BGraphics();
                result = Graphics.Initialize(Configuration, RenderForm.Handle);
            }

            BPerformanceTester.Initialize(
                "RenderForm C# SharpDX: " +
                Configuration.Width + "x" + Configuration.Height +
                " VSync:" + BSystemConfiguration.VerticalSyncEnabled +
                " FullScreen:" + BSystemConfiguration.FullScreen +
                "   " + RenderForm.Text,
                testTimeSeconds, Configuration.Width, Configuration.Height
            );

            // Create and initialize the FpsClass. 
            FPS = new BFPS();
            FPS.Initialize();

            // Create and initialize the CPU.
            CPU = new BCPU();
            CPU.Initialize();

            // Create and initialize Timer.
            Timer = new BTimer();
            if (!Timer.Initialize())
            {
                MessageBox.Show("Не удалось инициализтровать таймер.", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            // Create the sound object   sound01.wav
            Sound = new BWavaSound("sound01.wav");
            // Initialize the sound object.
            if (!Sound.Initialize(RenderForm.Handle))
            {
                MessageBox.Show("Не удалось инициализировать Direct Sound", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            // This seperates out the creation of one DirectSound object and
            // one PrimaryBuffer nad loads as many SecondaryBuffers as there
            // are Sound.LoadAudio Calls. We pass in only the first
            // instanceances DirectSound to play from both secondaryBuffers
            // from the one primaryBuffer.
            if (Sound.LoadAudio(Sound._DirectSound)) Sound.Play(0);

            // Create the position object.
            Position = new BPosition();

            return result;
        }

        private void InitializeWindow(string title)
        {
            int width = Screen.PrimaryScreen.Bounds.Width;
            int height = Screen.PrimaryScreen.Bounds.Height;

            // Initialize Window.
            RenderForm = new RenderForm(title)
            {
                ClientSize = new Size(Configuration.Width, Configuration.Height),
                FormBorderStyle = BSystemConfiguration.BorderStyle
            };

            // The form must be showing in order for the handle to be used in Input and Graphics objects.
            RenderForm.Show();
            RenderForm.Location = new Point((width / 2) - (Configuration.Width / 2), (height / 2) - (Configuration.Height / 2));
        }

        private void RunRenderForm()
        {
            RenderLoop.Run(RenderForm, () =>
            {
                if (!Frame())
                {
                    ShutDown();
                }
            });
        }

        public bool Frame()
        {
            // Check if the user pressed escape and wants to exit the application.
            if (!Input.Frame() || Input.IsEscapePressed()) return false;

            // Update the system stats.
            CPU.Frame();
            FPS.Frame();

            Timer.Frame2();
            if (BPerformanceTester.IsTimedTest)
            {
                BPerformanceTester.Frame(Timer.FrameTime);
                if (Timer.CumulativeFrameTime >= BPerformanceTester.TestTimeInSeconds * 1000)
                {
                    return false;
                }
            }

            // Get the location of the mouse from the input object.
            int mouseX, mouseY;
            Input.GetMouseLocation(out mouseX, out mouseY);

            // Set the frame time for calculating the updated position.
            Position.FrameTime = Timer.FrameTime;

            // Check if the left or right arrow key has been pressed, if so rotate the camera accordingly.
            bool keydown = Input.IsLeftArrowPressed();
            Position.TurnLeft(keydown);
            keydown = Input.IsRightArrowPressed();
            Position.TurnRight(keydown);
            keydown = Input.IsUpArrowPressed();
            Position.LookUp(keydown);
            keydown = Input.IsDownArrowPressed();
            Position.LookDown(keydown);

            // Do the frame processing for the graphics object.
            if (!Graphics.Frame(Position, FPS.Value, CPU.Value, Timer.FrameTime, mouseX, mouseY)) return false;

            // Finally render the graphics to the screen.
            if (!Graphics.Render(mouseX, mouseY)) return false;

            return true;
        }

        public void ShutDown()
        {
            ShutdownWindows();
            BPerformanceTester.ShutDown();

            // Release the position object.
            Position = null;
            // Release the Timer object
            Timer = null;
            // Release the FPS object
            FPS = null;
            // Release the CPU object
            CPU?.Shutdown();
            CPU = null;
            // Release the sound object
            Sound?.Shutdown();
            Sound = null;
            // Release graphics and related objects.
            Graphics?.Shutdown();
            Graphics = null;
            // Release DriectInput related object.
            Input?.Shutdown();
            Input = null;
            Configuration = null;
        }

        private void ShutdownWindows()
        {
            RenderForm?.Dispose();
            RenderForm = null;
        }
    }
}
