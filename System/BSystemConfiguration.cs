﻿using System.Windows.Forms;

namespace BenihanaEngineDX11.System
{
    class BSystemConfiguration
    {
        public string Title { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }

        // Static Properties
        public static bool FullScreen { get; private set; }
        public static bool VerticalSyncEnabled { get; private set; }
        public static float ScreenDepth { get; private set; }
        public static float ScreenNear { get; private set; }
        public static FormBorderStyle BorderStyle { get; set; }
        public static string ShaderFilePath { get; private set; }
        public static string SoundFilePath { get; private set; }
        public static string TextureFilePath { get; private set; }
        public static string ModelFilePath { get; set; }
        public static string FontFilePath { get; set; }

        // Constructors
        public BSystemConfiguration(bool fullScreen, bool vSync) : this("BenihanaEngine", fullScreen, vSync) { }
        public BSystemConfiguration(string title, bool fullScreen, bool vSync) : this(title, 800, 600, fullScreen, vSync) { }
        public BSystemConfiguration(string title, int width, int height, bool fullScreen, bool vSync)
        {
            FullScreen = fullScreen;
            Title = title;
            VerticalSyncEnabled = vSync;

            if (!FullScreen)
            {
                Width = width;
                Height = height;
            }
            else
            {
                Width = Screen.PrimaryScreen.Bounds.Width;
                Height = Screen.PrimaryScreen.Bounds.Height;
            }
        }

        // Static Constructor
        static BSystemConfiguration()
        {
            VerticalSyncEnabled = false;
            ScreenDepth = 1000.0f;
            ScreenNear = 0.1f;
            BorderStyle = FormBorderStyle.None;

            ShaderFilePath = @"Assets\Shaders\";
            TextureFilePath = @"Assets\Textures\";
            ModelFilePath = @"Assets\Models\";
            FontFilePath = @"Assets\Fonts\";
            SoundFilePath = @"Assets\Sounds\";
        }
    }
}
