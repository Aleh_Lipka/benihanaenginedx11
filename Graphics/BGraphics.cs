﻿using System;
using System.Windows.Forms;

using SharpDX;

using BenihanaEngineDX11.Graphics.Camera;
using BenihanaEngineDX11.Graphics.Data;
using BenihanaEngineDX11.Graphics.Models;
using BenihanaEngineDX11.Graphics.Shaders;
using BenihanaEngineDX11.Input;
using BenihanaEngineDX11.System;

namespace BenihanaEngineDX11.Graphics
{
    class BGraphics
    {
        private BDX11 D3D { get; set; }
        private BCamera Camera { get; set; }
        private BModel Model { get; set; }
        private BLightShader LightShader { get; set; }
        private BLight Light { get; set; }
        public BText Text { get; set; }
        private BModelList ModelList { get; set; }
        private BFrustum Frustum { get; set; }

        // Static properties
        public static float Rotation { get; set; }

        public BGraphics() { }

        public bool Initialize(BSystemConfiguration configuration, IntPtr windowHandle)
        {
            try
            {
                D3D = new BDX11();
                if (!D3D.Initialize(configuration, windowHandle)) return false;
                
                Camera = new BCamera();
                Camera.SetPosition(0, 0, -1);
                Camera.Render();
                var baseViewMatrix = Camera.ViewMatrix;

                // Create the text object.
                Text = new BText();
                if (!Text.Initialize(D3D.Device, D3D.DeviceContext, windowHandle, configuration.Width, configuration.Height, baseViewMatrix))
                    return false;

                // Create the model class.
                Model = new BModel();
                // Initialize the model object.
                if (!Model.Initialize(D3D.Device, "Sphere.txt", "Stone.bmp"))
                {
                    MessageBox.Show("Не удалось инициализировать модель.", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }

                // Create the light shader object.
                LightShader = new BLightShader();
                // Initialize the light shader object.
                if (!LightShader.Initialize(D3D.Device, windowHandle))
                {
                    MessageBox.Show("Не удалось инициализировать шейдер.", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }

                // Create the light object.
                Light = new BLight();
                // Iniialize the light object.  Changed to white in Tutorial 7
                // Added Setting of Ambiant Light to 15& brightness and adjust the Light direction along the X-Axis instead of the Z to see the ambient effect easier.
                Light.SetAmbientColor(0.15f, 0.15f, 0.15f, 1.0f);
                Light.SetDiffuseColor(1, 1, 1, 1);
                Light.SetDirection(0, 0.2f, 1);
                Light.SetSpecularColor(1, 1, 1, 1);
                Light.SetSpecularPower(10);

                // Create the model list object.
                ModelList = new BModelList();
                // Initialize the model list object.
                if (!ModelList.Initialize(25))
                {
                    MessageBox.Show("Не удалось инициализировать список моделей.", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }

                // Create the frustum object.
                Frustum = new BFrustum();

                return true;
            }
            catch(Exception ex)
            {
                MessageBox.Show("Не удалось инициализировать Direct3D\nОшибка: '" + ex.Message + "'", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        public void Shutdown()
        {
            // Release the frustum object.
            Frustum = null;
            // Release the light object.
            Light = null;
            Camera = null;

            // Release the model list object.
            ModelList?.Shutdown();
            ModelList = null;
            // Release the light shader object.
            LightShader?.ShutDown();
            LightShader = null;
            // Release the model object.
            Model?.ShutDown();
            Model = null;
            // Release the text object.
            Text?.Shutdown();
            Text = null;
            // Release the Direct3D object.
            D3D?.ShutDown();
            D3D = null;
        }

        public bool Frame(BPosition Position, int fps, int cpu, float frameTime, int mouseX, int mouseY)
        {
            // Set the frames per second.
            if (!Text.SetFps(fps, D3D.DeviceContext)) return false;
            // Set the cpu usage.
            if (!Text.SetCpu(cpu, D3D.DeviceContext)) return false;


            bool resultMouse = true, resultKeyboard = true;

            // Set the location of the mouse.
            if (!Text.SetMousePosition(mouseX, mouseY, D3D.DeviceContext)) resultMouse = false;
            
            // Set the position of the camera.
            Camera.SetPosition(0, 0, -10f);
            
            // Set the rotation of the camera.
            Camera.SetRotation(Position.RotationX, Position.RotationY, 0);

            return (resultMouse | resultKeyboard);
        }

        public bool Render(int mousePosX, int mousePosY)
        {
            // Clear the buffer to begin the scene.
            D3D.BeginScene(0f, 0f, 0f, 1.0f);
            // Generate the view matrix based on the camera position.
            Camera.Render();
            // Get the world, view, and projection matrices from camera and d3d objects.
            var viewMatrix = Camera.ViewMatrix;
            var worldMatrix = D3D.WorldMatrix;
            var projectionMatrix = D3D.ProjectionMatrix;
            var orthoMatrix = D3D.OrthoMatrix;
            // Construct the frustum.
            Frustum.ConstructFrustum(BSystemConfiguration.ScreenDepth, projectionMatrix, viewMatrix);
            // Initialize the count of the models that have been rendered.
            var renderCount = 0;
            Vector3 position;
            Vector4 color;
            // Go through all models and render them only if they can seen by the camera view.
            for (int index = 0; index < ModelList.ModelCount; index++)
            {
                // Get the position and color of the sphere model at this index.
                ModelList.GetData(index, out position, out color);
                // Before checking whether this model is in the view to render, adjust the position of the model to the newly rotated camera view to see if it needs to be rendered this frame or not.
                position = Vector3.TransformCoordinate(position, worldMatrix);
                // Set the radius of the sphere to 1.0 since this is already known.
                var radius = 1.0f;
                // Check if the sphere model is in the view frustum.
                bool renderModel = Frustum.CheckSphere(position, radius);
                // If it can be seen then render it, if not skip this model and check the next sphere.
                if (renderModel)
                {
                    // Move the model to the location it should be rendered at.
                    worldMatrix *= Matrix.Translation(position);
                    // Put the model vertex and index buffer on the graphics pipeline to prepare them for drawing.
                    Model.Render(D3D.DeviceContext);
                    // Render the model using the color shader.
                    if (!LightShader.Render(D3D.DeviceContext, Model.IndexCount, worldMatrix, viewMatrix, projectionMatrix, Model.Texture.TextureResource, Light.Direction, Light.AmbientColor, color, Camera.GetPosition(), Light.SpecularColor, Light.SpecularPower))
                        return false;
                    // Reset to the original world matrix.
                    worldMatrix = D3D.WorldMatrix * Matrix.RotationY(Rotation);
                    // Since this model was rendered then increase the count for this frame.
                    renderCount++;
                }
            }

            // Set the number of the models that was actually rendered this frame
            if (!Text.SetRenderCount(renderCount, D3D.DeviceContext)) return false;


            worldMatrix = D3D.WorldMatrix;
            // Turn off the Z buffer to begin all 2D rendering.
            D3D.TurnZBufferOff();
            // Turn on the alpha blending before rendering the text.
            D3D.TurnOnAlphaBlending();
            // Render the text string.
            if (!Text.Render(D3D.DeviceContext, worldMatrix, orthoMatrix)) return false;
            // Turn off the alpha blending before rendering the text.
            D3D.TurnOffAlphaBlending();
            // Turn on the Z buffer to begin all 2D rendering.
            D3D.TurnZBufferOn();
            // Present the rendered scene to the screen.
            D3D.EndScene();

            return true;
        }
    }
}
