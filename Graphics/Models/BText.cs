﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

using SharpDX;
using SharpDX.Direct3D;
using SharpDX.Direct3D11;
using SharpDX.DXGI;
using Buffer = SharpDX.Direct3D11.Buffer;
using Device = SharpDX.Direct3D11.Device;

using BenihanaEngineDX11.Graphics.Data;
using BenihanaEngineDX11.Graphics.Shaders;

namespace BenihanaEngineDX11.Graphics.Models
{
    class BText
    {
        // Structs
        [StructLayout(LayoutKind.Sequential)]
        public struct BSentence
        {
            public Buffer VertexBuffer;
            public Buffer IndexBuffer;
            public int VertexCount;
            public int IndexCount;
            public int MaxLength;
            public float red;
            public float green;
            public float blue;
            public string sentenceText;
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct BVertex
        {
            public Vector3 position;
            public Vector2 texture;
        }

        // Properties
        public BFont Font;
        public BFontShader FontShader;
        public int ScreenWidth;
        public int ScreenHeight;
        public Matrix BaseViewMatrix;
        public BSentence[] sentences = new BSentence[7];

        // Methods
        public bool Initialize(Device device, DeviceContext deviceContext, IntPtr windowHandle, int screanWidth, int screenHeight, Matrix baseViewMatrix)
        {
            // Store the screen width and height.
            ScreenWidth = screanWidth;
            ScreenHeight = screenHeight;
            // Store the base view matrix.
            BaseViewMatrix = baseViewMatrix;
            // Create the font object.
            Font = new BFont();
            // Initialize the font object.
            if (!Font.Initialize(device, "Fontdata.txt", "Font.bmp")) return false;
            // Create the font shader object.
            FontShader = new BFontShader();
            // Initialize the font shader object.
            if (!FontShader.Initialize(device, windowHandle)) return false;
            // Initialize the first sentence.
            if (!InitializeSentence(out sentences[0], 50, device)) return false;
            // Now update the sentence vertex buffer with the new string information.
            if (!UpdateSentece(ref sentences[0], "Benihana Engine v.0.0.12", 10, 10, 1, 1, 1, deviceContext)) return false;
            // Initialize the second sentence.
            if (!InitializeSentence(out sentences[1], 50, device)) return false;
            // Now update the sentence vertex buffer with the new string information.
            if (!UpdateSentece(ref sentences[1], "Debug Information:", 10, 30, 1, 1, 1, deviceContext)) return false;

            // Initialize the second sentence.
            if (!InitializeSentence(out sentences[2], 20, device)) return false;
            // Initialize the second sentence.
            if (!InitializeSentence(out sentences[3], 20, device)) return false;
            // Initialize the first sentence.
            if (!InitializeSentence(out sentences[4], 16, device)) return false;
            // Initialize the second sentence.
            if (!InitializeSentence(out sentences[5], 16, device)) return false;
            // Initialize the second sentence.
            if (!InitializeSentence(out sentences[6], 16, device)) return false;

            return true;
        }

        public void Shutdown()
        {
            // Release all sentances however many there may be.
            foreach (BSentence sentance in sentences)
            {
                ReleaseSentences(sentance);
            }
            sentences = null;
            // Release the font shader object.
            FontShader?.ShutDown();
            FontShader = null;
            // Release the font object.
            Font?.Shutdown();
            Font = null;
        }

        public bool Render(DeviceContext deviceContext, Matrix worldMatrix, Matrix orthoMatrix)
        {
            // Render all Sentances however many there mat be.
            foreach (BSentence sentance in sentences)
            {
                if (!RenderSentence(deviceContext, sentance, worldMatrix, orthoMatrix)) return false;
            }

            return true;
        }
        
        public bool SetMousePosition(int mouseX, int mouseY, DeviceContext deviceContext)
        {
            string mouseString;

            // Setup the mouseX string.
            mouseString = "Mouse X: " + mouseX.ToString();
            // Update the sentence vertex buffer with the new string information.
            if (!UpdateSentece(ref sentences[2], mouseString, 10, 50, 1, 1, 1, deviceContext)) return false;
            // Setup the mouseY string.
            mouseString = "Mouse Y: " + mouseY.ToString();
            // Update the sentence vertex buffer with the new string information.
            if (!UpdateSentece(ref sentences[3], mouseString, 10, 70, 1, 1, 1, deviceContext)) return false;
            
            return true;
        }

        public bool SetFps(int fps, DeviceContext deviceContext)
        {
            // Truncate the FPS to below 10,000
            if (fps > 9999) fps = 9999;
            // Convert the FPS integer to string format.
            string fpsString = string.Format("FPS: {0:d4}", fps);
            // Setup Colour variables with a default to white,  for assigning colour based on performance.
            float red = 1, green = 1, blue = 1;
            // If fps is 60 or above set the fps color to green.
            if (fps >= 60)
            {
                red = 0;
                green = 1;
                blue = 0;
            }
            // If fps is below 60 set the fps color to yellow
            if (fps < 60)
            {
                red = 1;
                green = 1;
                blue = 0;
            }
            // If fps is below 30 set the fps to red.
            if (fps < 30)
            {
                red = 1;
                green = 0;
                blue = 0;
            }
            // Update the sentence vertex buffer with the new string information.
            if (!UpdateSentece(ref sentences[4], fpsString, 10, 90, red, green, blue, deviceContext)) return false;

            return true;
        }

        public bool SetCpu(int cpu, DeviceContext deviceContext)
        {
            // Format string for this sentance to report CPU Usage percetange
            string formattedCpuUsage = string.Format("CPU: {0}%", cpu);
            // Update the sentence vertex buffer with the new string information.
            if (!UpdateSentece(ref sentences[5], formattedCpuUsage, 10, 110, 1, 1, 1, deviceContext)) return false;

            return true;
        }

        public bool SetRenderCount(int renderCount, DeviceContext deviceContext)
        {
            var renderCountString = string.Format("Render Count: {0}", renderCount);
            // Update the sentence vertex buffer with the new string information.
            if (!UpdateSentece(ref sentences[6], renderCountString, 10, 130, 1, 1, 1, deviceContext)) return false;

            return true;
        }

        private bool InitializeSentence(out BSentence sentence, int maxLength, Device device)
        {
            // Create a new sentence object.
            sentence = new BSentence();
            // Initialize the sentence buffers to null;
            sentence.VertexBuffer = null;
            sentence.IndexBuffer = null;
            // Set the maximum length of the sentence.
            sentence.MaxLength = maxLength;
            // Set the number of vertices in vertex array.
            sentence.VertexCount = 6 * maxLength;
            // Set the number of vertices in the vertex array.
            sentence.IndexCount = sentence.VertexCount;
            // Create the vertex array.
            var vertices = new BText.BVertex[sentence.VertexCount];
            // Create the index array.
            var indices = new int[sentence.IndexCount];
            // Initialize the index array.
            for (var i = 0; i < sentence.IndexCount; i++)
            {
                indices[i] = i;
            }

            // Set up the description of the dynamic vertex buffer.
            var vertexBufferDesc = new BufferDescription()
            {
                Usage = ResourceUsage.Dynamic,
                SizeInBytes = Utilities.SizeOf<BText.BVertex>() * sentence.VertexCount,
                BindFlags = BindFlags.VertexBuffer,
                CpuAccessFlags = CpuAccessFlags.Write,
                OptionFlags = ResourceOptionFlags.None,
                StructureByteStride = 0
            };

            // Create the vertex buffer.
            sentence.VertexBuffer = Buffer.Create(device, vertices, vertexBufferDesc);
            // Create the index buffer.
            sentence.IndexBuffer = Buffer.Create(device, BindFlags.IndexBuffer, indices);

            vertices = null;
            indices = null;

            return true;
        }

        private bool UpdateSentece(ref BSentence sentence, string text, int positionX, int positionY, float red, float green, float blue, DeviceContext deviceContext)
        {
            // Store the color of the sentence.
            sentence.red = red;
            sentence.green = green;
            sentence.blue = blue;
            // Get the number of the letter in the sentence.
            var numLetters = text.Length;
            // Check for possible buffer overflow.
            if (numLetters > sentence.MaxLength) return false;
            // Calculate the X and Y pixel position on screen to start drawing to.
            var drawX = -(ScreenWidth >> 1) + positionX;
            var drawY = (ScreenHeight >> 1) - positionY;
            // Use the font class to build the vertex array from the sentence text and sentence draw location.
            List<BText.BVertex> vertices;
            Font.BuildVertexArray(out vertices, text, drawX, drawY);

            DataStream mappedResource;

            #region Vertex Buffer
            // Lock the vertex buffer so it can be written to.
            deviceContext.MapSubresource(sentence.VertexBuffer, MapMode.WriteDiscard, SharpDX.Direct3D11.MapFlags.None, out mappedResource);
            // Copy the data into the vertex buffer.
            mappedResource.WriteRange<BText.BVertex>(vertices.ToArray());
            // Unlock the vertex buffer.
            deviceContext.UnmapSubresource(sentence.VertexBuffer, 0);
            #endregion

            vertices?.Clear();
            vertices = null;

            return true;
        }

        private void ReleaseSentences(BSentence sentence)
        {
            // Release the sentence vertex buffer.
            sentence.VertexBuffer?.Dispose();
            sentence.VertexBuffer = null;
            // Release the sentence index buffer.
            sentence.IndexBuffer?.Dispose();
            sentence.IndexBuffer = null;
        }

        private bool RenderSentence(DeviceContext deviceContext, BSentence sentence, Matrix worldMatrix, Matrix orthoMatrix)
        {
            // Set vertex buffer stride and offset.
            var stride = Utilities.SizeOf<BText.BVertex>();
            var offset = 0;
            // Set the vertex buffer to active in the input assembler so it can be rendered.
            deviceContext.InputAssembler.SetVertexBuffers(0, new VertexBufferBinding(sentence.VertexBuffer, stride, offset));
            // Set the index buffer to active in the input assembler so it can be rendered.
            deviceContext.InputAssembler.SetIndexBuffer(sentence.IndexBuffer, Format.R32_UInt, 0);
            // Set the type of the primitive that should be rendered from this vertex buffer, in this case triangles.
            deviceContext.InputAssembler.PrimitiveTopology = PrimitiveTopology.TriangleList;
            // Create a pixel color vector with the input sentence color.
            var pixelColor = new Vector4(sentence.red, sentence.green, sentence.blue, 1);
            // Render the text using the font shader.
            if (!FontShader.Render(deviceContext, sentence.IndexCount, worldMatrix, BaseViewMatrix, orthoMatrix, Font.Texture.TextureResource, pixelColor))
                return false;

            return true;
        }
    }
}
