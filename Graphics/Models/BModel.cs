﻿using System;
using System.Runtime.InteropServices;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using System.Globalization;

using SharpDX;
using SharpDX.D3DCompiler;
using SharpDX.Direct3D;
using SharpDX.Direct3D11;
using SharpDX.DXGI;
using SharpDX.Windows;
using Buffer = SharpDX.Direct3D11.Buffer;
using Device = SharpDX.Direct3D11.Device;

using BenihanaEngineDX11.System;
using BenihanaEngineDX11.Graphics.Data;
using BenihanaEngineDX11.Graphics.Shaders;

namespace BenihanaEngineDX11.Graphics.Models
{
    public class BModel
    {
        private CultureInfo cultureInfo = CultureInfo.CreateSpecificCulture("en-US");

        // Structures
        [StructLayout(LayoutKind.Sequential)]
        public struct BModelFormat
        {
            public float x, y, z;
            public float tu, tv;
            public float nx, ny, nz;
        }

        // Properties
        private Buffer VertexBuffer { get; set; }
        private Buffer IndexBuffer { get; set; }
        private int VertexCount { get; set; }
        public int IndexCount { get; set; }
        public BTexture Texture { get; set; }
        public BModelFormat[] ModelObject { get; private set; }

        // Constructor
        public BModel() { }

        // Methods
        public bool Initialize(Device device, string modelFormatFilename, string textureFileName)
        {
            if (!LoadModel(modelFormatFilename)) return false;
            if (!InitializeBuffer(device)) return false;
            if (!LoadTexture(device, textureFileName)) return false;

            return true;
        }

        private bool LoadModel(string modelFormatFilename)
        {
            modelFormatFilename = BSystemConfiguration.ModelFilePath + modelFormatFilename;
            List<string> lines = null;

            try
            {
                lines = File.ReadLines(modelFormatFilename).ToList();

                var vertexCountString = lines[0].Split(new char[] { ':' })[1].Trim();
                VertexCount = int.Parse(vertexCountString);
                IndexCount = VertexCount;
                ModelObject = new BModelFormat[VertexCount];

                for (var i = 4; i < lines.Count && i < 4 + VertexCount; i++)
                {
                    var modelArray = lines[i].Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);

                    ModelObject[i - 4] = new BModelFormat()
                    {
                        x = float.Parse(modelArray[0], NumberStyles.Float, cultureInfo),
                        y = float.Parse(modelArray[1], NumberStyles.Float, cultureInfo),
                        z = float.Parse(modelArray[2], NumberStyles.Float, cultureInfo),
                        tu = float.Parse(modelArray[3], NumberStyles.Float, cultureInfo),
                        tv = float.Parse(modelArray[4], NumberStyles.Float, cultureInfo),
                        nx = float.Parse(modelArray[5], NumberStyles.Float, cultureInfo),
                        ny = float.Parse(modelArray[6], NumberStyles.Float, cultureInfo),
                        nz = float.Parse(modelArray[7], NumberStyles.Float, cultureInfo)
                    };
                }

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private bool LoadTexture(Device device, string textureFileName)
        {
            textureFileName = BSystemConfiguration.TextureFilePath + textureFileName;
            Texture = new BTexture();
            Texture.Initialize(device, textureFileName);

            return true;
        }

        private bool InitializeBuffer(Device device)
        {
            try
            {
                // Create the vertex array.
                var vertices = new BLightShader.BVertex[VertexCount];
                // Create the index array.
                var indices = new int[IndexCount];

                for (var i = 0; i < VertexCount; i++)
                {
                    vertices[i] = new BLightShader.BVertex()
                    {
                        position = new Vector3(ModelObject[i].x, ModelObject[i].y, ModelObject[i].z),
                        texture = new Vector2(ModelObject[i].tu, ModelObject[i].tv),
                        normal = new Vector3(ModelObject[i].nx, ModelObject[i].ny, ModelObject[i].nz)
                    };

                    indices[i] = i;
                }

                // Create the vertex buffer.
                VertexBuffer = Buffer.Create(device, BindFlags.VertexBuffer, vertices);
                // Create the index buffer.
                IndexBuffer = Buffer.Create(device, BindFlags.IndexBuffer, indices);

                return true;
            }
            catch
            {
                return false;
            }
        }
        
        public void ShutDown()
        {
            ReleaseTexture();
            ShutDownBuffers();
            ReleaseModel();
        }

        private void ReleaseModel()
        {
            ModelObject = null;
        }

        private void ReleaseTexture()
        {
            Texture?.ShutDown();
            Texture = null;
        }

        private void ShutDownBuffers()
        {
            // Release the index buffer.
            IndexBuffer?.Dispose();
            IndexBuffer = null;
            // Release the vertex buffer.
            VertexBuffer?.Dispose();
            VertexBuffer = null;
        }

        public void Render(DeviceContext deviceContext)
        {
            // Put the vertex and index buffers on the graphics pipeline to prepare for drawings.
            RenderBuffers(deviceContext);
        }
        
        private void RenderBuffers(DeviceContext deviceContext)
        {
            // Set the vertex buffer to active in the input assembler so it can be rendered.
            deviceContext.InputAssembler.SetVertexBuffers(0, new VertexBufferBinding(VertexBuffer, Utilities.SizeOf<BLightShader.BVertex>(), 0));
            // Set the index buffer to active in the input assembler so it can be rendered.
            deviceContext.InputAssembler.SetIndexBuffer(IndexBuffer, Format.R32_UInt, 0);
            // Set the type of the primitive that should be rendered from this vertex buffer, in this case triangles.
            deviceContext.InputAssembler.PrimitiveTopology = PrimitiveTopology.TriangleList;
        }
    }
}
